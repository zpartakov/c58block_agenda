# c58block_agenda
[![c58block_agenda](//radeff.red/pics/web/block_agenda.png)](c58block_agenda)

This concrete5 block allow a page list with begin/end date (calendar/agenda) (works on c5.8, to try on c5.7); only the coming date are listed, past events are ignored

To use on multilingual websites

Version: 1.0.0
Date: 2017-06-29
 
Developer: Fred Radeff <fradeff@akademia.ch>



## Install notes
- create first 3 pages attributes with following handles: agenda_startdate, agenda_enddate, lieu (lieu is the place the event occurs)
- create / adapt paths for your calendars and adapt view.php (change: "/fr/actualites/agenda" & "/de/aktuell/agenda")
- install as a standard c5 block

## Demo:

* https://uniterre.ch/fr/actualites/agenda
* https://uniterre.ch/de/aktuell/agenda

## License: GPL v3
