<?php
###########################################################################
# display events from calendar based on ad hoc                            #
# attributes start / end event                                            #
# Fred Radeff (aka FR), fradeff@akademia.ch, radeff.red                   #
# required: concrete5.8                                                   #
###########################################################################
$th = Loader::helper('text');
$c = Page::getCurrentPage();
$dh = Core::make('helper/date'); /* @var $dh \Concrete\Core\Localization\Service\Date */

//language of the current page
$ms = \Concrete\Core\Multilingual\Page\Section\Section::getBySectionOfSite($c);
if (is_object($ms)) {
   $lalangue= $ms->getLocale();
}
if($lalangue=="fr_FR") {
	$chemin="/fr/actualites/agenda";
} else {
	$chemin="/de/aktuell/agenda";
}

//begin page list building
$list = new \Concrete\Core\Page\PageList();
$list->filterByPath($chemin);
$list->filterByAttribute('agenda_startdate', date('Y-m-d H:i:s', $start), ">=");
$pages = $list->getResults();

//declare arrays
$titreArray=array();
$agenda_startdateArray=array();
$agenda_enddateArray=array();
$lieuArray=array();
$descriptionArray=array();
$urlArray=array();

foreach ($pages as $page):
	array_push($titreArray, $th->entities($page->getCollectionName()));
	array_push($descriptionArray, $page->getCollectionDescription());
	array_push($agenda_startdateArray, $page->getAttribute('agenda_startdate'));
	array_push($agenda_enddateArray, $page->getAttribute('agenda_enddate'));
  array_push($lieuArray, $page->getAttribute('lieu'));
  $url = URL::to($page);
  //echo $url."<br>";
  array_push($urlArray, $url);

endforeach;
//exit;
$size=count($titreArray);

//sort by enddate
array_multisort($agenda_startdateArray, $descriptionArray, $agenda_enddateArray,$titreArray,$lieuArray,$urlArray);

//loop on sorted array
for ($i=0; $i <= $size; $i++) {
		$title = $titreArray[$i];
    $agenda_startdate = $agenda_startdateArray[$i];
		$agenda_enddate = $agenda_enddateArray[$i];
    $datestartUnix = date('U',strtotime($agenda_startdate));
    $lieu = $lieuArray[$i];
    $aujourdhui = date('U');
    $dateendUnix = date('U',strtotime($agenda_enddate));
    $description = $descriptionArray[$i];
    $url = $urlArray[$i];

		if($aujourdhui<=$dateendUnix) {
		  $datefintest=$dateendUnix;
		}else {
		  $datefintest=$datestartUnix;
		}

		if($aujourdhui<=$datefintest) {
				setlocale (LC_ALL, $lalangue);
				$dateStart = strftime('%A, %d %B %G, %H:%M',strtotime($agenda_startdate));
				$dateStart=utf8_encode($dateStart);
				if(strlen($agenda_enddate)>0) {
					if(($dateendUnix-$datestartUnix)<(24*3600)) { //same day
						$dateEnd = " - " .strftime('%H:%M',strtotime($agenda_enddate));
					} else { //more than 24 hours later
						$dateEnd = " - " .strftime('%A, %d %B %G, %H:%M',strtotime($agenda_enddate));
					}
					//$dateEnd .= "<br>datestartUnix: $datestartUnix; dateEndUnix: $dateendUnix; aujourdhui: " .$aujourdhui ." ; ecart: " .($aujourdhui-$datefintest); //tests

				} else {
					$dateEnd="";
				}
				$dateEnd=utf8_encode($dateEnd);

				?>
				<div class="ccm-block-page-list-page-entry-text">
						<div class="ccm-block-page-list-title">
							<div class="zagtitle">
								<a href="<?php echo $url ?>"><?php echo $title ?></a>
							</div>
						</div>
							 <div class="zagdate"><?php echo $dateStart; ?></div>
							 <div class="zagdate"><?php echo $dateEnd; ?></div>
							 <div class="zaglieu"><?php echo $lieu; ?></div>
					</div>
					<?php
			}
}
?>
